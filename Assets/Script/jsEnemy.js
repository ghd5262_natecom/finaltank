﻿#pragma strict

private var power = 12000;

var bullet	: Transform;
var target	: Transform;
var spPoint	: Transform;
var explosion : Transform;
var snd		: AudioClip;

private var ftime : float = 0.0;


var tank : GameObject;

function Start()
{
	tank = GameObject.FindGameObjectWithTag( "TANK" );
}

function Update () {
	transform.LookAt(tank.transform);
	ftime += Time.deltaTime;
	
	var hit : RaycastHit;
	var fwd = transform.TransformDirection(Vector3.forward);
	Debug.DrawRay(spPoint.transform.position, fwd * 20, Color.green);
	if(Physics.Raycast(spPoint.transform.position, fwd, hit, 20) == false)return;
	if(hit.collider.gameObject.tag != "TANK" || ftime < 1)return;
	Debug.Log(hit.collider.gameObject.name);
	Instantiate(explosion, spPoint.transform.position, spPoint.transform.rotation);
	
	var obj = Instantiate(bullet, spPoint.transform.position, Quaternion.identity);
	obj.rigidbody.AddForce(fwd * power);
	ftime = 0;
	//AudioSource.PlayClipAtPoint(snd, spPoint.transform.position);
}