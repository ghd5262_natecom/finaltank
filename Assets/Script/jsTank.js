﻿#pragma strict

var speed = 5;
var rotSpeed = 120;
var turret : GameObject;
var power = 1200;
var bullet : Transform;
var explosion : Transform;
var bomb : Transform;
var snd : AudioClip;
static var BombNumber=5;

function Start(){
	BombNumber = 5;
}

function Update () {
	var amtToMove = speed * Time.deltaTime;
	var amtToRot = rotSpeed * Time.deltaTime;
	
	var front = Input.GetAxis("Vertical");
	var ang = Input.GetAxis("Horizontal");
	var ang2 = Input.GetAxis("FinalTank");
	
	transform.Translate(Vector3.forward * front * amtToMove);
	transform.Rotate(Vector3(0, ang * amtToRot, 0));
	turret.transform.Rotate(Vector3.up * ang2 * amtToRot);
	
	if(Input.GetButtonDown("Fire1")){
		
		var spPoint = GameObject.Find("spawnPoint");
		AudioSource.PlayClipAtPoint(snd, spPoint.transform.position);
		Instantiate(explosion, spPoint.transform.position, Quaternion.identity);
		
		var myBullet = Instantiate(bullet, spPoint.transform.position, Quaternion.identity);
		myBullet.rigidbody.AddForce(spPoint.transform.forward * power);
	}
	else if(Input.GetButtonDown("Fire2")){
		if(BombNumber > 0){
			var spPoint2 = GameObject.Find("spawnPoint5");
			Instantiate(explosion, spPoint2.transform.position, Quaternion.identity);
			var myBomb = Instantiate(bomb, spPoint2.transform.position, Quaternion.identity);
			myBomb.rigidbody.AddForce(spPoint2.transform.forward * power);
			BombNumber--;
		}
	}
}
function OnTriggerEnter(coll : Collider){
	if(coll.gameObject.tag == "ITEM" && BombNumber < 3){
		Destroy(coll.gameObject);
		BombNumber += 3;
	}
}