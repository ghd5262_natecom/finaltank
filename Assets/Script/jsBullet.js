﻿#pragma strict


var explosion : Transform;

function OnTriggerEnter(coll : Collider){
	
	Instantiate(explosion, transform.position, Quaternion.identity);
	
	Destroy(gameObject);
	if(coll.gameObject.tag == "ENEMY"){
		jsScore.hit += 10;
		Destroy(coll.transform.root.gameObject);
	}
	else if(coll.gameObject.tag == "TANK"){
		jsScore.lose++;
	}
}