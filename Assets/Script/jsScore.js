﻿#pragma strict

static var hit = 0;
static var lose = 0;
static var myHp = 100;
static var Score = 0;

function Start(){
	hit = 0;
	lose = 0;
	myHp = 100;
}

function OnGUI(){
	GUI.Label(Rect (10, 10, 120, 20), "명중 : " + hit);
	GUI.Label(Rect (10, 30, 120, 20), "피격 : " + lose);
	GUI.Label(Rect (10, 50, 120, 20), "체력 : " + myHp);
	GUI.Label(Rect (10, 70, 120, 20), "시간 : " + jsPlayTime.ftime);
	GUI.Label(Rect (10, 120, 120, 20), "폭탄 : " + jsTank.BombNumber);
	Score = (hit * 10) + (myHp * 10) - (lose * 15);	
}