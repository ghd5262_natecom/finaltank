﻿#pragma strict
var snd : AudioClip;
var explosion : Transform;
var explosionScale : Transform;

function OnTriggerEnter(coll : Collider){
	Instantiate(explosion, transform.position, Quaternion.identity);
	AudioSource.PlayClipAtPoint(snd, transform.position);
	Destroy(gameObject);
	Instantiate(explosionScale, transform.position, Quaternion.identity );
}