﻿#pragma strict

function OnCollisionEnter( coll : Collision )
{
	if(coll.gameObject.tag == "MYHOUSE"){
		Destroy(gameObject);
		jsScore.myHp -= 5;
		if(jsScore.myHp < 5){
			Application.LoadLevel("LoseGame");
		}
	}
}
