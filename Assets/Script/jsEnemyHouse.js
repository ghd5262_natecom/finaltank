﻿#pragma strict

var Enemy : Transform;
var spPoint : Transform;
private var ftime : float = 0.0;

function Update () {
	ftime += Time.deltaTime;
	if(ftime < 3)return;
	var obj = Instantiate(Enemy, spPoint.transform.position, Quaternion.identity);
	ftime = 0;
}